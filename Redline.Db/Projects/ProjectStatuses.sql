﻿CREATE TABLE [dbo].[ProjectStatuses]
(
    [Id] INT NOT NULL,
    [Status] NVARCHAR(50) NOT NULL CHECK([Status] IN ('in development', 'in staging', 'in production'))
)
