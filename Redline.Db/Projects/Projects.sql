﻿CREATE TABLE [dbo].[Projects]
(
    [Id] UNIQUEIDENTIFIER NOT NULL,
    [UserId] UNIQUEIDENTIFIER NOT NULL,
    [Name] NVARCHAR(100) NOT NULL,
    [Description] NVARCHAR(500) NOT NULL,
    [CreatedOn] DATETIME2 DEFAULT(sysutcdatetime()) NOT NULL,
    CONSTRAINT [PK_Projects_Id] PRIMARY KEY (Id),
    CONSTRAINT [FK_Projects_UserId] FOREIGN KEY (UserId) REFERENCES [dbo].[Users] (Id) ON DELETE CASCADE,
    CONSTRAINT [UQ_Projects_Name] UNIQUE (Name)
)
