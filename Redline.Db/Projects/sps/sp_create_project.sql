﻿CREATE PROCEDURE [dbo].[sp_create_project]
    @id UNIQUEIDENTIFIER,
    @userId UNIQUEIDENTIFIER,
    @name NVARCHAR(100),
    @description NVARCHAR(500)
AS
    INSERT INTO Projects(Id, UserId, Name, Description)
    VALUES(@id, @userId, @name, @description)
