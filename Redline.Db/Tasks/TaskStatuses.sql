﻿CREATE TABLE [dbo].[TaskStatuses]
(
    [Id] INT NOT NULL,
    [Status] NVARCHAR(50) NOT NULL CHECK([Status] IN ('open','blocked','planned', 'awaited', 'in progress', 'test ready', 'in testing', 'completed'))
)
