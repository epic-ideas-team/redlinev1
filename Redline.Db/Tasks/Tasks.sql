﻿CREATE TABLE [dbo].[Tasks]
(
    [Id] UNIQUEIDENTIFIER                                                   NOT NULL,
    [ProjectId] UNIQUEIDENTIFIER                                            NOT NULL,
    [Title] NVARCHAR(100)                                                   NOT NULL,
    [Description] NVARCHAR(500)                                             NULL,
    [TimeSpent] TIME DEFAULT('00:00:00')                                    NOT NULL,
    [TimeBudget] TIME DEFAULT('00:00:00')                                   NOT NULL,
    [CreatedOn] DATETIME2 DEFAULT(sysutcdatetime())                         NOT NULL,

    CONSTRAINT [PK_Tasks_Id] PRIMARY KEY (Id),
    CONSTRAINT [FK_Tasks_ProjectId] FOREIGN KEY (ProjectId) REFERENCES [dbo].[Projects] (Id) ON DELETE CASCADE,
    CONSTRAINT [UQ_Tasks_Title] UNIQUE (Title)
)
