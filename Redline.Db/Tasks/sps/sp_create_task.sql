﻿CREATE PROCEDURE [dbo].[sp_create_task]
    @id UNIQUEIDENTIFIER,
    @projectId UNIQUEIDENTIFIER,
    @title NVARCHAR(100),
    @description NVARCHAR(500),
    @timeSpent TIME = '00:00:00',
    @timeBudget TIME
AS
    INSERT INTO Tasks(Id, ProjectId, Title, Description, TimeSpent, TimeBudget)
    VALUES(@id, @projectId, @title, @description, @timeSpent, @timeBudget)
