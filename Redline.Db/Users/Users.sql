﻿CREATE TABLE [dbo].[Users]
(
    [Id] UNIQUEIDENTIFIER                                           NOT NULL,
    [UserLogin] NVARCHAR(50)                                            NOT NULL,
    [FullName] NVARCHAR(100)                                       NULL,
    [Email] VARCHAR(320)                                            NOT NULL,
    [PasswordHash] NVARCHAR(128)                                    NOT NULL,
    [PasswordSalt] NVARCHAR(128)                                    NOT NULL,
    [CreatedOn] DATETIME2 DEFAULT(sysutcdatetime())                 NOT NULL,

    CONSTRAINT [PK_Users_Id] PRIMARY KEY (Id),
    CONSTRAINT [UQ_Users_Login] UNIQUE (UserLogin),
    CONSTRAINT [UQ_Email_Email] UNIQUE (Email)
)
