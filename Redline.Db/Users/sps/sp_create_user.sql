﻿CREATE PROCEDURE [dbo].[sp_create_user]
    @id UNIQUEIDENTIFIER,
    @login NVARCHAR(50) = NULL,
    @fullName NVARCHAR(100),
    @passHash NVARCHAR(128),
    @passSalt NVARCHAR(128),
    @email VARCHAR(320)
AS
    INSERT INTO Users(Id, UserLogin, FullName, Email, PasswordHash, PasswordSalt)
    VALUES(@id, @login, @fullName, @email, @passHash, @passSalt)
