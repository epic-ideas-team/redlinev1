﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Redline.Web1.Features.Home
{
    public class Greetings
    {
        public Greetings(string greeting)
        {
            Greeting = greeting;
        }

        public string Greeting { get; set; }
    }
}
