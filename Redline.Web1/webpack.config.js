﻿var path = require('path');
var webpack = require('webpack');

module.exports = {
    entry: {
        main: "./"
    },

    output: {
        filename: "/wwwroot/bundle/bundle.js",
        publicPath: "/wwwroot/bundle"
    },

    resolve: {
        extensions: [

        ]
    },

    devtool: "source-map",

    module: {
        loaders: [

        ]
    },

    plugins: [

    ]
};