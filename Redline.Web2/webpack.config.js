﻿const _path = require('path');
const _webpack = require('webpack');
const _extractTextPlugin = require('extract-text-webpack-plugin');
const _plugin_checker = require('awesome-typescript-loader').CheckerPlugin;
const _webpack_merge = require('webpack-merge');


module.exports = (environment) => {
    const _isDevBuild = !(environment && environment.prod);

    /*configuration that is included in both client-side and server-side bundles configs*/
    const _sharedConfig = () => ({
        stats: {
            modules: false
        },

        resolve: {
            extensions: ['.js', '.jsx', '.ts', '.tsx']
        },

        output: {
            fulename: "react-app.[hash].js",
            publicPath: "bundle/"
        },
        module: {
            rules: [
                { test: /\.tsx?$/, include: "/Client/", use: 'awesome-typescript-loader?silent=true' },
                { test: /\.(png|jpg|jpeg|gif|svg)$/, use: 'url-loader?limit=25000' }
            ]
        },
        plugins: [new _plugin_checker()]
    });

    // Configuration for client-side bundle suitable for running in browsers
    const _directory_client_bundle = "./wwwroot/bundle";
    const _clientBundleConfig = _webpack_merge(_sharedConfig(), {
        entry: {
            "client-side": "./Client/client-side.browser.tsx"
        },
        module: {
            rules: [
                { test: /\.css$/, use: _extractTextPlugin.extract({ use: _isDevBuild ? "css-loader" : "css-loader?minimize" }) }
            ]
        },
        output: {
            path: _path.join(__dirname, _directory_client_bundle)
        },
        plugins: [
            new _extractTextPlugin("site.css"),
            new _webpack.DllReferencePlugin({
                context: __dirname,
                manifest: require('./wwwroot/bundle/client-manifest.json')
            })
        ].concat(_isDevBuild ? [
            new _webpack.SourceMapDevToolPlugin({
                filename: "[file].map",
                moduleFilenameTemplate: _path.relative(_directory_client_bundle, "[resourcePath]")
            })
        ] : [
                new _webpack.optimize.UglifyJsPlugin()
            ])
    });

    const _serverBundleConfig = _webpack_merge(_sharedConfig(), {
        resolve: {
            mainFields: ['main']
        },

        entry: {
            "server-side": "./Client/server-side.server.tsx",
        },

        plugins: [
            new _webpack.DllReferencePlugin({
                context: __dirname,
                manifest: require("./Client/bundle/server-manifest.json"),
                sourceType: "commonjs2",
                name: "./bundle"
            })
        ],
        output: {
            libraryTarget: "commonjs",
            path: _path.join(__dirname, "./Client/bundle")
        },
        target: "node",
        devtool: "inline-source-map"
    });

    return [_clientBundleConfig, _serverBundleConfig];
}