﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MainModuleShared } from './main.module.shared';
import { RootComponent } from './root/root.component';

@NgModule({
    bootstrap: [RootComponent],
    imports: [
        BrowserModule,
        MainModuleShared,
    ],
    providers: [
        { provide: 'BASE_URL', useFactory: getBaseUrl }
    ]
})
export class MainModule {

}

export function getBaseUrl(): string {
    return document.getElementsByTagName('base')[0].href;
}