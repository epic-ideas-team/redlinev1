﻿import { NgModule } from '@angular/core';
import { ServerModule } from '@angular/platform-server';
import { MainModuleShared } from './main.module.shared';
import { RootComponent } from './root/root.component';

@NgModule({
    bootstrap: [RootComponent],
    imports: [
        ServerModule,
        MainModuleShared
    ]
})
export class MainModule {
}