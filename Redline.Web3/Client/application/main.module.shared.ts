﻿/*
Used in app.module.browser.ts and app.module.server.ts
*/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { RootComponent } from './root/root.component';
import { NavBarComponent } from './navbar/navbar.component';

@NgModule({
    declarations: [
        RootComponent,
        NavBarComponent
    ],
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' }
        ])
    ]
})
export class MainModuleShared { }