﻿using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.AspNetCore.Mvc.Razor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Redline.Web3.Configuration
{
    public class FeaturesConvention : IControllerModelConvention
    {
        private const string features = "features";

        public void Apply(ControllerModel controller)
        {
            controller.Properties.Add(features, GetFeatureName(controller.ControllerType.FullName));
        }

        private string GetFeatureName(string fullName)
        {
            var tokens = fullName.Split('.');

            if (!tokens.Any(t => t.ToLower() == features))
            {
                return string.Empty;
            }

            return tokens
                .SkipWhile(t => !t.Equals(features, StringComparison.InvariantCultureIgnoreCase))
                .Skip(1)
                .Take(1)
                .FirstOrDefault();
        }
    }

    public class FeatureFolderRazorViewEngine : IViewLocationExpander
    {
        private const string features = "features";

        public IEnumerable<string> ExpandViewLocations(ViewLocationExpanderContext context, IEnumerable<string> viewLocations)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (viewLocations == null)
            {
                throw new ArgumentNullException(nameof(viewLocations));
            }

            var controllerActionDescriptor = context.ActionContext.ActionDescriptor as Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor;

            if (controllerActionDescriptor == null)
            {
                throw new NullReferenceException("ControllerActionDescriptor cannot be null.");
            }

            var featureName = controllerActionDescriptor.Properties[features] as string;

            foreach (var location in viewLocations)
            {
                yield return location.Replace("{3}", featureName);
            }
        }

        public void PopulateValues(ViewLocationExpanderContext context) { }
    }
}
