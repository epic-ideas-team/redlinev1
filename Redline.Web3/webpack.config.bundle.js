﻿const _path = require('path');
const _webpack = require('webpack');
const _plugin_extract_text = require('extract-text-webpack-plugin');
const _webpack_merge = require('webpack-merge');

const treeShakableModules = [
    '@angular/animations',
    '@angular/common',
    '@angular/compiler',
    '@angular/core',
    '@angular/forms',
    '@angular/http',
    '@angular/platform-browser',
    '@angular/platform-browser-dynamic',
    '@angular/router',
    'zone.js',
];

const nonTreeShakableModules = [
    'popper.js',
    'bootstrap',
    'bootstrap/dist/css/bootstrap.css',
    'es6-promise',
    'es6-shim',
    'event-source-polyfill',
    'jquery',
];

const allModules = treeShakableModules.concat(nonTreeShakableModules);

module.exports = (env) => {
    const _extractCss = new _plugin_extract_text("bundle.css");
    const _isDevBuild = !(env && env.prod);
    const _sharedConfig = {
        stats: {
            modules: false
        },
        resolve: {
            extensions: ['.js']
        },
        module: {
            rules: [
                { test: /\.(png|woff|woff2|eot|ttf|svg)(\?|$)/, use: 'url-loader?limit=100000' }
            ]
        },
        output: {
            publicPath: 'bundle/',
            filename: '[name].js',
            library: '[name]_[hash]'
        },
        plugins: [
            new _webpack.ProvidePlugin({ $: 'jquery', jQuery: 'jquery' }),
            new _webpack.ContextReplacementPlugin(/\@angular\b.*\b(bundles|linker)/, _path.join(__dirname, './Client')),
            new _webpack.ContextReplacementPlugin(/angular(\\|\/)core(\\|\/)@angular/, _path.join(__dirname, './Client')),
            new _webpack.IgnorePlugin(/^vertx$/)
        ]
    };

    const _clientBundleConfig = _webpack_merge(_sharedConfig, {
        entry: {
            bundle: _isDevBuild ? allModules : nonTreeShakableModules
        },
        output: {
            path: _path.join(__dirname, 'wwwroot', 'bundle')
        },
        module: {
            rules: [
                { test: /\.css(\?|$)/, use: _extractCss.extract({ use: _isDevBuild ? 'css-loader' : 'css-loader?minimize' }) }
            ]
        },
        plugins: [
            _extractCss,
            new _webpack.DllPlugin({
                path: _path.join(__dirname, 'wwwroot', 'bundle', '[name]-manifest.json'),
                name: '[name]_[hash]'
            })
        ].concat(_isDevBuild ? [] : [
            new _webpack.optimize.UglifyJsPlugin()
        ])
    });

    const _serverBundleConfig = _webpack_merge(_sharedConfig, {
        target: "node",
        resolve: {
            mainFields: ['main']
        },
        entry: {
            bundle: allModules.concat(["aspnet-prerendering"])
        },
        output: {
            path: _path.join(__dirname, 'Client', 'bundle'),
            libraryTarget: 'commonjs2',
        },
        module: {
            rules: [{ test: /\.css(\?|$)/, use: ['to-string-loader', _isDevBuild ? 'css-loader' : 'css-loader?minimize'] }]
        },
        plugins: [
            new _webpack.DllPlugin({
                path: _path.join(__dirname, 'Client', 'bundle', '[name]-manifest.json'),
                name: '[name]_[hash]'
            })
        ]
    });

    return [_clientBundleConfig, _serverBundleConfig];
}