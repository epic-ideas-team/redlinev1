﻿const _path = require('path');
const _webpack = require('webpack');
const _webpack_merge = require('webpack-merge');
const _plugin_extract_text = require('extract-text-webpack-plugin');
const _plugin_checker = require('awesome-typescript-loader').CheckerPlugin;
const _plugin_aot = require('@ngtools/webpack').AotPlugin;
const _plugin_html_webpack = require('html-webpack-plugin');

module.exports = (env) => {

    /*configuration that is included in both client-side and server-side bundles configs*/
    const isDevBuild = !(env && env.prod);
    const _sharedConfiguration = {
        stats: {
            modules: false                          // Adds/removes built modules information
        },
        context: __dirname,                         // string (absolute path!)
        // the home directory for webpack
        // the entry and module.rules.loader option
        // is resolved relative to this directory


        resolve: {                              //These options change how modules are resolved.
            extensions: [".js", ".ts"]             // Automatically resolve certain extensions.
        },
        output: {                               // The top-level output key contains set of options instructing webpack on how and where it should output your bundles, assets and anything else you bundle or load with webpack.
            filename: "[name].js",      // determines the name of each output bundle.
            publicPath: "bundle/"                   //
        },
        module: {
            rules: [
                { test: /\.ts$/, include: /Client/, use: isDevBuild ? ['awesome-typescript-loader?silent=true', 'angular2-template-loader'] : '@ngtools/webpack' },
                { test: /\.html$/, use: isDevBuild ? 'html-loader?minimize=false' : 'html-loader?minimize=true' },
                { test: /\.css$/, use: ['to-string-loader', isDevBuild ? 'css-loader' : 'css-loader?minimize'] },
                { test: /\.(png|jpg|jpeg|gif|svg)$/, use: 'url-loader?limit=25000' }
            ]
        },
        plugins: [new _plugin_checker()]
    };

    // Configuration for client-side bundle suitable for running in browsers
    const _directory_client_bundle = "./wwwroot/bundle";
    const _clientSideBundleConfig = _webpack_merge(_sharedConfiguration, {
        entry: {
            "client-side": "./Client/client-side.browser.ts"
        },
        output: {
            path: _path.join(__dirname, _directory_client_bundle)
        },
        plugins: [
            new _webpack.DllReferencePlugin({
                context: __dirname,
                manifest: require("./wwwroot/bundle/bundle-manifest.json")
            })
        ].concat(isDevBuild ? [
            new _webpack.SourceMapDevToolPlugin({
                filename: '[file].map',
                moduleFilenameTemplate: _path.relative(_directory_client_bundle, "[resourcePath]")
            })
        ]
            :
            [
                new _webpack.optimize.UglifyJsPlugin(),
                new _plugin_aot({
                    tsConfigPath: "./tsconfig.json",
                    entryModule: _path.join(__dirname, "Client/application/main.module.browser#MainModule"),
                    exclude: ['./**/*.server.ts']
                })
            ])
    });


    // Configuration for server-side (prerendering) bundle suitable for running in Node
    const _serverSideBundleConfig = _webpack_merge(_sharedConfiguration, {
        resolve: {
            mainFields: ['main']
        },
        entry: {
            "server-side": "./Client/server-side.server.ts"
        },
        plugins: [
            new _webpack.DllReferencePlugin({
                context: __dirname,
                manifest: "./Client/bundle/bundle-manifest.json",
                sourceType: "commonjs2",
                name: "./bundle"
            })
        ].concat(isDevBuild ? [] : [
            new _plugin_aot({
                tsConfigPath: "./tsconfig.json",
                entryModule: _path.join(__dirname, "Client/application/main.module.server#MainModule"),
                exclude: ['./**/*.browser.ts']
            })
            ]),
        output: {
            libraryTarget: "commonjs",
            path: _path.join(__dirname, "./Client/bundle")
        },
        target: 'node',
        devtool: "inline-source-map"
    });

    return [_clientSideBundleConfig, _serverSideBundleConfig];
};